module.exports = {
    host:process.env.BILLING_SERVICE_REDIS_HOST,
    port: process.env.BILLING_SERVICE_REDIS_PORT,
};
