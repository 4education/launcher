module.exports = {
    SUPER_ADMIN: "root",
    ADMIN: "admin",
    COMPANY_ADMIN: "c_admin",
    COMPANY_EMPLOYEE: "c_employee",
    COMPANY: "company",
    CUSTOMER: "customer",
    GUEST: "guest",
    INTERNAL: "internal", // for internal communication
};