const {
	NODE_ENV,
	LAUNCHER_SERVICE_HOST,
	LAUNCHER_SERVICE_PORT,
} = process.env;

module.exports = {
	"serviceName": "educational",
	"protocol": NODE_ENV == 'production' ? 'https://' : 'http://',
    "host": LAUNCHER_SERVICE_HOST,
    "port": LAUNCHER_SERVICE_PORT,
	"origin": `${LAUNCHER_SERVICE_HOST}${LAUNCHER_SERVICE_PORT ? `:${LAUNCHER_SERVICE_PORT}` : ''}`,
};