module.exports = {
    db:"mongodb", //this is static data for connection using in mongoose library
    host:process.env.LAUNCHER_SERVICE_DB_HOST,
    port: process.env.LAUNCHER_SERVICE_DB_PORT,
    dbName:process.env.LAUNCHER_SERVICE_DB_NAME
};
