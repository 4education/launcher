const express = require('express');
const bodyParser = require('body-parser');
const mongooseLib = require("./libs/mongoose");

//run service registration;
require("./RegisterServices");

const serviceContainer = require("./libs/ServiceContainer");

const apiRouterService = serviceContainer.getService('internal', 'routerService');

// Configuration 
const dbConf = require('./config/db');
const server = require('./config/server');

mongooseLib.connect(dbConf.db, dbConf.host, dbConf.dbName, dbConf.port)
    .catch((e) => { console.log("DB CONNECT ERROR: "); console.log(e); });

mongooseLib.setHandlerOnce('open', () => {
    console.log("DB open");
    new AppServer().run();
});


class AppServer
{
    constructor()
    {
        this.express = express();
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
    }

    run()
    {
        //register api
        this.setHeaders();
        this.setRoutes();
        this.express.listen(server.port, () => {
            console.log('Server started on port ' + server.port);
        });
    }

    setRoutes()
    {
        apiRouterService.initMiddleWares(this.express);
        apiRouterService.buildApiRoutes();
        this.express.use('/api', apiRouterService.getApiRoutes());
    }

    setHeaders()
    {
        this.express.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Access-Token, X-Access-Type, Content-Type, Accept");
            res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH");
            next();
        });
    }
}