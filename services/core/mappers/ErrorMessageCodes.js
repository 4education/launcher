module.exports = {
    OAUTH: {
        NOT_AUTHORIZE:          {code:"oauth.1.0", message:"User should be authorize"}
    },
    PERMISSION: {
        NOT_ALLOWED:            {code:"perm.1.0", message:"User don't have permission"},
        SERVICE_NOT_PAID:       {code:"perm.1.1", message:"You don't have subscription plan or your plan expired"},
        SERVICE_REACHED_LIMIT:  {code:"perm.1.2",  message:"You reached your limit for %s numbers"}
    }
};