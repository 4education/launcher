const mongoose = require('../../../libs/mongoose').mongoose;
const NotFoundEntityException =require("../exceptions/NotFoundEntityException");

class FilteredCollectionModel
{

    __getLookup()
    {
        throw new Error("__getLookup should be declare in the child");
    }

    getProvider()
    {
        throw new Error("getProvider should be declare in the child");
    }

    executeQuery(requestQuery)
    {
        let pagging = [];

        if(this.__getLookup())
        {
            pagging.push({$lookup: this.__getLookup()});
        }

        pagging.push({ $match: requestQuery.whereConditions });

        pagging.push({$sort: Object.keys(requestQuery.sortBy).length > 0 && requestQuery.sortBy || {_id: -1} });

        if(Object.keys(requestQuery.fields).length > 0)
        {
            pagging.push({$project: requestQuery.fields});
        }

        if(this.__getCustomFieldsToResponse())
        {
            pagging.push({$addFields: this.__getCustomFieldsToResponse()});
        }

        if(requestQuery.unwind)
        {
            pagging.push({ $unwind : requestQuery.unwind});
        }

        if(requestQuery.groupBy && Object.keys(requestQuery.groupBy).length > 0)
        {
            pagging.push({$group: requestQuery.groupBy});
        }

        let facet =  { $facet: {
                totalCount: [ { $count: "total" }],
                items: [{$skip: 0}]
            } };

        if(requestQuery.paginationParams && requestQuery.paginationParams.limit)
        {
            facet.$facet.items = [
                { $skip: requestQuery.paginationParams.skip },
                { $limit: requestQuery.paginationParams.limit }
            ];
        }
        pagging.push(facet);
        return new Promise( (resolve, reject) => {
            this.getProvider().aggregate(pagging).exec((err, collection) => {
                if(err) return reject(err);

                return resolve(JSON.parse(JSON.stringify(collection))[0] || []);
            });
        });
    }

    __getCustomFieldsToResponse()
    {
        return null;
    }

    validateMongoDBId(id)
    {
        if( !mongoose.Types.ObjectId.isValid(id) )
        {
            throw new NotFoundEntityException();
        }
    }
}

module.exports = FilteredCollectionModel;
