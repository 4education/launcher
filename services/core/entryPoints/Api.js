const exceptionMapper = require("../mappers/ExceptionApiStatusMapper");
const RequestQuery = require("../../../libs/RequestQuery");
const mongoose = require('../../../libs/mongoose').mongoose;


class Api
{
    __getMethod(name)
    {
        let method = name;
        return (req, res) => {
            try{
                let ctrlMethod = this[method](req, res);
                if(ctrlMethod && ctrlMethod.catch)
                {
                    ctrlMethod.catch(exception => {
                        this.__sendFailedResponse(req, res, exception);
                    })
                }
            }
            catch(exception)
            {
                this.__sendFailedResponse(req, res, exception);
            }
        };
    }

    mapEntityToClient(data, mapper = null)
    {
        if(mapper)
        {
            return mapper.toClientProperty(data);
        }
        return data;
	}
	
	mapCollectionToClientBySingle(obj, mapper) {
		return this.mapEntityToClient(obj, mapper)
	}

    mapCollectionToClient(collData, mapper)
    {
        let data = [];
        for(let i = 0; i < collData.length; i++)
        {
            data.push(this.mapEntityToClient(collData[i], mapper));
        }
        return data;
    }

    __sendFailedResponse(req, res, exception)
    {
        res.status(new exceptionMapper().getApiStatus(exception)).send(
            { message: exception.message }
        );
        console.log(exception, exception.stack.split("\n"));
    }

    getRequestQueryObject(req)
    {
        let q = new RequestQuery();
        return this.__prepareRequest(req, q);
    }

    getPaginationParams(req)
    {
        let pageData = {
            limit: parseInt(req.query.limit) || 50,
            offset: parseInt(req.query.offset) || 0
        };
        return pageData;
    }

    __prepareRequest(req, RequestQuery)
    {
        let paginationParams = this.getPaginationParams(req);

        RequestQuery.setPagination(paginationParams.offset, paginationParams.limit);

        if(req.query.fields)
        {
            try{
                let fields = JSON.parse(req.query.fields);
                if(!Array.isArray(fields))
                {
                    this.send400Error(res, "Incorrect data for fields, should be array");
                    return false;
                }
                RequestQuery.setSelectFields(fields);
            }catch (e)
            {
                console.log(e, e.stack.split("\n"));
                this.send400Error(res, "Incorrect data for fields, should be array");
                return false;
            }
        }

        if(req.query.orderBy)
        {
            let fieldRequestMapper = this.__getFieldRequestMapper();
            try{
                let orderBy = JSON.parse(req.query.orderBy);
                for(let field in orderBy)
                {
                    if(!orderBy.hasOwnProperty(field))
                    {
                        continue;
                    }
                    let val = orderBy[field];
                    field = fieldRequestMapper[field] || field;
                    RequestQuery.addSortBy(field, val);
                }
            }catch (e)
            {
                console.log(e, e.stack.split("\n"));
                throw new Error("Incorrect data for orderBy parameter");
            }
        }
        if(req.query.filter)
        {
            const fieldRequestMapper = this.__getFieldRequestMapper(),
                  valueRequestMapper = this.__getValueRequestMapper();
            try{
                let filters = JSON.parse(req.query.filter);
                for(let field in filters)
                {
                    if(!filters.hasOwnProperty(field))
                    {
                        continue;
                    }
                    const value = valueRequestMapper[field] && valueRequestMapper[field](filters[field]) || filters[field];
                    field = fieldRequestMapper[field] || field;
                    console.log(field, value);
                    RequestQuery.addWhereConditions(field, value);
                }
            }catch (e)
            {
                console.log(e, e.stack.split("\n"));
                this.send400Error(res, "Incorrect data for filter parameter");
                return false;
            }
        }

        return RequestQuery;
    }

    __getValueRequestMapper() {
        return {
            search(value) {
                return { like: { pattern: '.*'+value+'.*', options: 'i' } };
            }
        }
    }

    __getFieldRequestMapper()
    {
        return {
            search: 'name'
        };
	}
	
    setCollectionResponseData(dataCollection, mapper)
    {
        if(!dataCollection || !dataCollection.totalCount)
        {
            return {
                'items': [],
                'totalCount': 0
            };
        }
        let total = 0;
        if(dataCollection.totalCount[0] && dataCollection.totalCount[0].total)
        {
            total = dataCollection.totalCount[0].total;
        }
        return {
            'items': this.mapCollectionToClient(dataCollection.items || [], mapper),
            'totalCount': total
        };
    }

    __getUserFilter(req)
    {
        //return {uid: {$in: [req.user.uid, null]},
        //return {cid: {$in: [mongoose.Types.ObjectId(req.user.cid)]}};
        return {};
    }
    __addUserFilterForFilteredCollection(RequestQuery, req)
    {
        //return {uid: {$in: [req.user.uid, null]},
        RequestQuery.addWhereConditions('cid', {in: [mongoose.Types.ObjectId(req.user.cid)]});
    }

    __addUidCidToObject(obj, req)
    {
        //obj.uid = req.user.uid;
        obj.cid = req.user.cid;
    }
}

module.exports = Api;
