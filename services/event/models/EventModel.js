const statuses = ['pending', 'inProcess', 'finished'];

class EventModel
{
    constructor(eventName, data = null)
    {
        this.eventName = eventName;
        this.eventData = data;
        this.status = 'pending';
    }

    getName()
    {
        return this.eventName;
    }

    setToInProgress()
    {
        this.status = 'inProcess'
    }

    finish()
    {
        this.status = 'finished'
    }

    getData()
    {
        return this.eventData;
    }

    getEventStaticData()
    {
        return {
            type: this.eventName,
            status: this.status,
            data: this.eventData
        }
    }
}

module.exports = EventModel;