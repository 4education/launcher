const axios = require('axios');
const config = require("../config/content_service_cof");

class InternalApi
{
    async getLesson(uid,lid)
    {
        let { data } = await this.__sendData({uid, lid}, '/internal/get_lesson');
        return data;
    }

    async getUserCourses(uid, pagination)
    {
        let { data } = await this.__sendData({uid, pagination}, '/internal/groups_users_list');
        return data;
    }

    async getGroupsInList(list, cid, pagination)
    {
        let { data } = await this.__sendData({groups:{in:list}, pagination, cid}, '/internal/groups_users_list');
        return data;
    }

    async getGroupsNotInList(list, cid, pagination)
    {
        let { data } = await this.__sendData({groups:{not_in:list}, pagination, cid}, '/internal/groups_users_list');
        return data;
    }

    async getUserGroups(uid)
    {
        let { data } = await this.__sendData({uid}, '/user/groups');
        return data;
    }

    async __sendData(data, reqUriPart)
    {
        try {
            let headers = {};
            headers[config.secret_key_header_name] = config.secret_key_communication;
            return axios.post(config.url+reqUriPart, data, {
                headers
            });
        }catch (e) {
            console.log('error communication with content service', e);
            return;
        }
    }
}

module.exports = InternalApi;