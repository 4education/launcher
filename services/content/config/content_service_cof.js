const protocol = process.env.NODE_ENV == 'production' ? 'https://' : 'http://';
module.exports = {
    "protocol": protocol,
    "host":process.env.CONTENT_SERVICE_HOST,
    "port":process.env.CONTENT_SERVICE_PORT,
    "url": protocol+process.env.CONTENT_SERVICE_HOST+ (process.env.CONTENT_SERVICE_PORT ? ':'+process.env.CONTENT_SERVICE_PORT : '') +'/api',
    "secret_key_header_name": process.env.SECRET_KEY_HEADER_NAME,
    "secret_key_communication": process.env.SECRET_KEY_COMMUNICATION
};