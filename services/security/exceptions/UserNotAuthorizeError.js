const InternalError = require("./InternalError");

class UserNotAuthorizeError extends InternalError
{

}

module.exports = UserNotAuthorizeError;