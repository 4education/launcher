const allowRoles = require("./config/system_roles");
const service_api_map = require("./config/service_api_map");
const role_permissions = require("./config/role_permissions");
const UserNotAuthorizeError = require("./exceptions/UserNotAuthorizeError");
const RoleDontHavePermissionError = require("./exceptions/RoleDontHavePermissionError");
const errorsCodes = require("../core/mappers/ErrorMessageCodes");

class SecurityService
{
    /**
     * Apply all restictions for public api
     *
     * @param {Object} req  request object from express nodejs module.
     */
    async processApiPermissions(req)
    {
        let routeUrl = this.__prepareKey(req.method, req.route.path);
        const matchUrlData = service_api_map[routeUrl] || service_api_map[req.method.toLowerCase()+'_*'];
        if(matchUrlData)
        {
            if(this.checkIsUserNeedAuthorized(matchUrlData) && (!req.user || !req.user.role))
            {
                throw new UserNotAuthorizeError(errorsCodes.OAUTH.NOT_AUTHORIZE);
            }

            let role = req.user && req.user.role ? req.user.role : allowRoles.GUEST;

            if(!this.checkIsUserHavePermission(matchUrlData.service, role, matchUrlData.permission))
            {
                throw new RoleDontHavePermissionError(errorsCodes.PERMISSION.NOT_ALLOWED);
            }

            return this.runConditions(matchUrlData, req);
        }
    }

    checkIsUserHavePermission(serviceName, role, permission = null)
    {
        if(role_permissions[role] && role_permissions[role][serviceName] && permission)
        {
            return (role_permissions[role][serviceName] == permission || role_permissions[role][serviceName] == 'rw');
        }
        //if restrictions is not set that mean allow
        return true;
    }

    checkIsUserNeedAuthorized(apiRestrictions)
    {
        return (apiRestrictions.shouldAuthentificate && apiRestrictions.shouldAuthentificate === true);
    }

    runConditions(apiRestrictions, req)
    {
        if(typeof apiRestrictions.conditionFunction === 'function')
        {
            return apiRestrictions.conditionFunction(req);
        }
    }

    __prepareKey(method, routePath)
    {
        return method.toLowerCase()+'_'+routePath;
    }
}

module.exports = new SecurityService();