const allowRoles = require("../../../config/system_roles");
const RoleDontHavePermissionError = require("../exceptions/RoleDontHavePermissionError");
const serviceContainer = require("../../../libs/ServiceContainer");
const redislib = require('../../../libs/RedisClient');
const { serviceName } = require('../../../config/server');
const errorsCodes = require("../../core/mappers/ErrorMessageCodes");

let rolesAllowCreateManualy = [allowRoles.ADMIN, allowRoles.SUPER_ADMIN];

const getUserPlan = (user) => {
    return new Promise((resolve,reject) => {
            redislib.client.get(user.cid+'_'+serviceName, (error, result) => {
                if (error) {
                    return reject(new RoleDontHavePermissionError(errorsCodes.PERMISSION.SERVICE_NOT_PAID));
                }
                if(!result)
                {
                    return reject(new RoleDontHavePermissionError(errorsCodes.PERMISSION.SERVICE_NOT_PAID));
                }
                const accountData = JSON.parse(result);
                let curDate = new Date().setHours(0,0,0,0);
                let expiredDate = new Date(accountData.expiredDate).setHours(0,0,0,0);
                if(curDate >= expiredDate)
                {
                    return reject(new RoleDontHavePermissionError(errorsCodes.PERMISSION.SERVICE_NOT_PAID));
                }
                resolve(JSON.parse(result));
            });
    });
};
const checkIsEmployee = (req) => {
    if(req.user && [allowRoles.INTERNAL, allowRoles.SUPER_ADMIN, allowRoles.ADMIN, allowRoles.COMPANY_EMPLOYEE, allowRoles.COMPANY_ADMIN].indexOf(req.user.role) === -1)
    {
        throw new RoleDontHavePermissionError(errorsCodes.PERMISSION.NOT_ALLOWED);
    }
};
const getUserStatisticInfo = async (user) => {
    return serviceContainer.getService('internal', 'statistic').getByCid(user.cid)
};

module.exports = {
    "get_*": {service: "testsService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            //const {offerInfo} = await getUserPlan(req.user);
        }},

    //CATEGORIES
    "get_/categories": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "get_/categories/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "post_/categories": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "patch_/categories/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "delete_/categories/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},

    //COURSES
    "get_/courses": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "get_/courses/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "post_/courses": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "patch_/courses/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "delete_/courses/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},


    //MODULES
    "get_/modules": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "get_/modules/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "post_/modules": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "patch_/modules/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "delete_/modules/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},


    //LESSONS
    "get_/lessons": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "get_/lessons/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "post_/lessons": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "patch_/lessons/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
    "delete_/lessons/:id": {service: "contentService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }},
};