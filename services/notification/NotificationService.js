
const Email = require("./services/Email");


class NotificationService
{
    sendEmail(config, emailTo, emailFrom, subject, text)
    {
        return new Promise((resolve, reject) => {
            this.__getEmailService(config).sendEmail(emailTo, emailFrom, subject, text)
                .then(info => {
                resolve(info);
            }).catch(err => {
                reject(err);
            })
        });
    }

    sendEmailWithHtmlTpl(config, emailTo, emailFrom, subject, htmlTplPath, htmlTplData)
    {
        return new Promise((resolve, reject) => {
            this.__getEmailService(config).sendEmailWithHtmlTpl(emailTo, emailFrom, subject, htmlTplPath, htmlTplData)
                .then(info => {
                    resolve(info);
                }).catch(err => {
                reject(err);
            })
        });
    }

    __getEmailService(config)
    {
        return new Email(config);
    }
}

module.exports = new NotificationService();