const nodemailer = require('nodemailer');
const EmailTemplate = require('email-templates');

class Email
{
    constructor(conf)
    {
		//TODO:: validation
		console.log(conf);
        this.transporter =  nodemailer.createTransport(conf);
    }


    sendEmail(emailTo, emailFrom, subject, text)
    {
        let mailOptions =  {
            from: emailFrom, // sender address
            to: emailTo, // list of receivers
            subject: subject, // Subject line
            text: text, // plain text body
        };

        return this.transporter.sendMail(mailOptions);
    }

    sendEmailWithHtmlTpl(emailTo, emailFrom, subject, htmlTplPath, htmlTplData)
    {
        let mailOptions =  {
            from: emailFrom, // sender address
            to: emailTo, // list of receivers
            subject: subject, // Subject line
		};

        let tplOptions =  {
            message: mailOptions, // sender address
            htmlToText: false
		};
		
		console.log(mailOptions);
		console.log(tplOptions);

        let email = new EmailTemplate(tplOptions);
        return new Promise((resolve, reject) => {
            email.render(htmlTplPath+'/html.pug', htmlTplData).then(data => {
                mailOptions.html = data;
                mailOptions.text = data;
                this.transporter.sendMail(mailOptions).then(data => {resolve(data)}).catch(err => { reject(err)})
            }).catch(err => { reject(err)})
        });
    }
}

module.exports = Email;