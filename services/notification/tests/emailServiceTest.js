//sending email test
const mailerService = require("../NotificationService");
const path = require('path');

let config = {
    sendmail: true,
    newline: 'unix',
    path: '/usr/sbin/sendmail'
};
let fromEmail = "support@4quiz.org";

describe('sending emails tests using sendmail tronsporter',(done) => {
    it('should be sending an email', (done) => {
        mailerService.sendEmail(config, "support@4quiz.org", fromEmail, 'Sorry it is test', "sorry :)").then(info => {
            console.log(info.response);
            console.log(info.envelope);
            done();
        }).catch(err => {
            done(err);
        })
    });

    //TODO:: tests for email with template
    it('should be sending an email with html template', (done) => {
        mailerService.sendEmailWithHtmlTpl(config, "support@4quiz.org", fromEmail,'Sorry it is test', path.join(__dirname, 'emailTpls'), {some: "Some text"}).then(info => {
            console.log(info);
            console.log(info.envelope);
            done();
        }).catch(err => {
            done(err);
        })
    });
});