const NotificationService = require("../NotificationService");

class Internal
{

    constructor(emailConfig)
    {
        //TODO:: validation
        this.emailConf = emailConfig;
    }

    sendMailByTpl(to, sub, templatePath, templateData)
    {
        return NotificationService.sendEmailWithHtmlTpl(this.emailConf.config, to, this.emailConf.fromEmail, sub, templatePath, templateData)
    }

    sendMail(to, sub, text)
    {
        return NotificationService.sendEmail(this.emailConf.config, to, this.emailConf.fromEmail, sub, text);
    }

    sendMailToAdmin(from, sub, text)
    {
        return NotificationService.sendEmail(this.emailConf.config, this.emailConf.fromEmail, from, sub, text);
    }
}

module.exports = Internal;