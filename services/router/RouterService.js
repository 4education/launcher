
const serviceContainer = require("../../libs/ServiceContainer");

const allowsApiMethods = ['get', 'put', 'post', 'patch', 'delete', 'head'];
const exp = require('express');

class RouterService
{
    constructor()
    {
        this.routes = {api: []};
        this.middleWares = [];
    }

    buildApiRoutes()
    {
        let api = exp.Router();
        let ApiServices = serviceContainer.getServicesByType('api');

        for(let serviceName in ApiServices)
        {
            if(!ApiServices.hasOwnProperty(serviceName))
            {
                continue;
            }

            if(typeof ApiServices[serviceName].getSchema !== 'function')
            {
                console.log(serviceName+" does not have method schema");
                continue;
            }
            let schema = ApiServices[serviceName].getSchema();

            if(!schema)
            {
                console.log(serviceName+" have not valid schema. It should be object");
                continue;
            }

            for(let method in schema)
            {
                if(!schema.hasOwnProperty(method))
                {
                    continue;
                }

                if(allowsApiMethods.indexOf(method) === -1)
                {
                    console.log(method+" in schema of "+ serviceName +" NOT allowed");
                    continue;
                }

                if(typeof schema[method] !== 'object')
                {
                    console.log(serviceName+" should be object");
                    continue;
                }

                for(let url in schema[method])
                {
                    if(!schema[method].hasOwnProperty(url) || !schema[method][url].act)
                    {
                        continue;
                    }
                    if(this.middleWares && this.middleWares.length > 0)
                    {
                        api[method](url, ...this.middleWares, schema[method][url].act);
                    }
                    else{
                        api[method](url, schema[method][url].act);
                    }
                }
            }
        }
        this.routes.api = api;
    }

    getApiRoutes()
    {
        return this.routes.api;
    }

    setMiddleWareFunction(middleWareFunction)
    {
        if(typeof middleWareFunction !== 'function')
        {
            throw new Error("MiddleWare should be function");
        }
        this.middleWares.push(middleWareFunction);
    }

    initMiddleWares(express)
    {
        // console.log("start setting middleWares");
        // for(let i = 0; i < this.middleWares.length; i++)
        // {
        //     express.use(this.middleWares[i]);
        // }
        // console.log("middleWares were set");
    }
}

module.exports = new RouterService();