module.exports = {
    "jwt_secret_key": process.env.JWT_SECRET_KEY, //for token generation
    "jwt_expires_in": process.env.JWT_EXPIRED_IN, //86400 - expires in 24 hours
};