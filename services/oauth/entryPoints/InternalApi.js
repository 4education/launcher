const oauthService = require("../OauthService");


class InternalApi
{
    async decryptData(token)
    {
        return new oauthService().decryptTokenData(token);
    }

    async addUserDataToRequest(req, res, next)
    {
        const authHeader = req.headers['x-access-token'];
        if(authHeader)
        {
            try{
                req.user = await new oauthService().decryptTokenData(authHeader);
            }catch(e)
            {
                console.log(e);
                return res.status(401).send(
                    { message: "Not authorize" }
                );
            }
        }
        next();
    }
}

module.exports = InternalApi;