const Mapper = require("../../../libs/Mapper");

class ApiMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "uid",
                toClient: "uid",
                required: true,
                type: "string",
            },
            {
                toServer: "courseId",
                toClient: "courseId",
                required: true,
                type: "string",
            },
            {
                toServer: "moduleId",
                toClient: "moduleId",
                required: true,
                type: "string",
            },
            {
                toServer: "lessonId",
                toClient: "lessonId",
                required: true,
                type: "string",
            },
            {
                toServer: "materialSectionData",
                toClient: "materialSectionData",
                toServerSkipped: true,
                type: "array",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            },
            {
                toServer: "updatedDate",
                toClient: "updatedDate",
                toServerSkipped: true,
                type: "string",
            }
        ];
    }
}

module.exports = ApiMapper;
