const Mapper = require("../../../libs/Mapper");

class ApiMaterialSectionMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "secId",
                toClient: "secId",
                required: true,
                type: "string",
            },
            {
                toServer: "data",
                toClient: "data"
            }
        ];
    }
}

module.exports = ApiMaterialSectionMapper;
