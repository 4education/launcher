const ThemesService = require("../ThemesService");


class InternalApi
{
    getById(id)
    {
        return new ThemesService().getItem(id);
    }
}

module.exports = InternalApi;