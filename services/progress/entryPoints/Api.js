const ProgressService = require("../ProgressService");
const serviceContainer = require("../../../libs/ServiceContainer");
const RequestQuery = require("../../../libs/RequestQuery");

const ApiMapper = require("../mappers/ApiMapper");
const ApiMaterialSectionMapper = require("../mappers/ApiMaterialSectionMapper");


const CoreApi = require("../../core/entryPoints/Api");

class Api extends CoreApi
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "get": {
                "/course/:cid/module/:mid/lesson/:lid": {
                    act: this.__getMethod('getLesson')
                },
                "/progress/my": {
                    act: this.__getMethod('getAll')
                },
                "/progress/my/course/:id": {
                    act: this.__getMethod('getItem')
                },
                "/progress/all_users/course/:id": {
                    act: this.__getMethod('getItem')
                },
            },
            "post": {
                "/course/:cid/module/:mid/lesson/:lid/set_progress": {
                    act: this.__getMethod('setProgress')
                }
            }
        };
    }

    /**
     * Returns todolist collection
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getAll(req, res)
    {

        const requestObject = this.getRequestQueryObject(req);
        this.__addUserFilterForFilteredCollection(requestObject, req);
        let list = await new ThemesService().getThemesFilteredCollection(requestObject);
        if(!list)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }

        res.status(200).send(
            this.setCollectionResponseData(list, new ApiMapper())
        );
    }

    /**
     * Returns todolist item or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getLesson(req, res)
    {
        let lesson = await serviceContainer.getService('internal','contentService').getLesson(req.user.uid,req.params.lid);

        if(!lesson)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        let progress = await new ProgressService().getLessonProgress(this.__getUserFilter(req)).getItem(req.params.id);
        lesson.progress = this.mapEntityToClient(progress, new ApiMapper())
        res.status(200).send(
            lesson
        );
    }

    /**
     * Returns todolist item or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async getMyProgress(req, res)
    {
        let requestObject = new RequestQuery();

        requestObject.addGroupByStage( {
            _id: "$courseId",
            passedNumbers:{"$sum": { "$size": "$materialSectionData" }}
        });

        let progress = await new ProgressService().getFilteredCollection(requestObject);
        let mappedData = {}
        progress.items.map((item) => {
            mappedData[item._id] = item.passedNumbers;
        });

        let courses = await serviceContainer.getService('internal','contentService')
            .getUserCourses(req.user.uid,this.getPaginationParams());
        courses.items = courses.items.map((item) => {
            if(mappedData[item.id])
            {
                item.statData.passedNumbers = mappedData[item.id];
            }
            return item;
        });
        res.status(200).send(
            courses
        );
    }

    /**
     * Create new item and returns created item data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async setProgress(req, res)
    {
        let mappedObject = new ApiMaterialSectionMapper().toServerProperty(req.body);
        mappedObject.uid = req.user.uid;
        let item = await new ProgressService().updateCreateItem(
            mappedObject,
            req.user.uid,
            req.params.cid,
            req.params.mid,
            req.params.lid,
            );

        res.status(200).send(
            this.mapEntityToClient(item, new ApiMapper() )
        );
    }

    /**
     * Update existing item in list and returns updated item data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.uid  User uid.
     */
    async updateItem(req, res)
    {
        let mappedObject = new ApiMapper().toServerProperty(req.body);

        let item = await new ThemesService().addUserDataFilter(this.__getUserFilter(req)).updateItem(req.params.id, mappedObject);
        if(!item)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            this.mapEntityToClient(item, new ApiMapper())
        );
    }

    /**
     * Delete existing item and returns 201 status no content or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.uid  User uid.
     */
    async deleteItem(req, res)
    {
        let result = await new ThemesService().addUserDataFilter(this.__getUserFilter(req)).deleteItem(req.params.id);
        if(!result)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(201).send();
    }
}

module.exports = Api;
