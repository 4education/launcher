//this wrapper should incoupsulate all business logic
const ProgressModel = require("./models/Progress");
const NotFoundEntityException = require("./exceptions/NotFoundEntityException");
const InternalErrorException = require("./exceptions/InternalError");
const serviceContainer = require("../../libs/ServiceContainer");
const events = require("./config/Events");

class ProgressService
{
    getAll()
    {
        return this.__getModel().getAll();
    }

    getLessonProgress(uid, lid)
    {
        return this.__getModel().getLessonProgress(uid, lid);
    }

    getFilteredCollection (requestObject)
    {
        return this.__getModel().executeQuery(requestObject);
    }

    async updateCreateItem(sectionData, uid, cid, mid, lid)
    {
        let item = await this.__getModel().createUpdateSection(sectionData, uid, cid, mid, lid);
        if(!updatedItem)
        {
            throw new InternalErrorException("something wrong");
        }

        return updatedItem.toObject() ;
    }

    addUserDataFilter(userData)
    {
        this.userTofilter = userData;
        return this;
    }

    __getModel()
    {
        return new ProgressModel();
    }

    __getEventManager()
    {
        return  serviceContainer.getService('internal', 'eventService');
    }
}

module.exports = ProgressService;
