const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');

// define the User model schema
const ProgressSchema = new mongoose.Schema({
    createdDate:{ type: Date, default: Date.now},
    updatedDate:{ type: Date, default: null},
    uid:{ type: mongoose.Schema.Types.ObjectId, index: true }, //user_id
    courseId: { type: mongoose.Schema.Types.ObjectId, index: true }, //course id
    moduleId: { type: mongoose.Schema.Types.ObjectId, index: true }, //module id
    lessonId: { type: mongoose.Schema.Types.ObjectId, index: true }, //lesson id
    materialSectionData: [new mongoose.Schema({
        secId:{ type: mongoose.Schema.Types.ObjectId, index: true, unique:true },
        data:{type: mongoose.Schema.Types.Mixed, default: {}}
    })]
});

const provider = mongoose.model('users_progresses', ProgressSchema);

class ThemeModel extends FilteredCollectionModel
{
    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }

    create(itemData)
    {
        let data = {
            courseId: itemData.courseId,
            moduleId: itemData.moduleId,
            lessonId: itemData.lessonId,
            uid: itemData.uid,
            materialSectionData: itemData.materialSectionData || [],
        };
        return provider.create(data);
    }

    getLessonProgress(uid, lid)
    {
        return provider.findOne(this.getAdditionalConditions(
            {
                lessonId: mongoose.Types.ObjectId(lid),
                uid: mongoose.Types.ObjectId(uid),
            })).exec();
    }

    remove(id)
    {
        this.validateMongoDBId(id);
        return provider.find(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).remove().exec();
    }

    getById(id)
    {
        this.validateMongoDBId(id);
        return provider.findOne(this.getAdditionalConditions({_id: mongoose.Types.ObjectId(id)})).exec();
    }

    createSectionData(sectionData, uid, cid, mid, lid)
    {
        let data = {
            courseId: cid,
            moduleId: mid,
            lessonId: lid,
            uid: uid
        };
        return provider.findOneAndUpdate(
            {
                uid: mongoose.Types.ObjectId(uid),
                cid: mongoose.Types.ObjectId(cid),
                mid: mongoose.Types.ObjectId(mid),
                lid: mongoose.Types.ObjectId(lid),
            },
            { $push: {"materialSectionData": sectionData}, $set: data},{new: true, upsert: true}).exec();
    }

    async createUpdateSection(sectionData, uid, cid, mid, lid)
    {
        let res = await provider.findOneAndUpdate(
            {
                uid: mongoose.Types.ObjectId(uid),
                cid: mongoose.Types.ObjectId(cid),
                mid: mongoose.Types.ObjectId(mid),
                lid: mongoose.Types.ObjectId(lid),
                "materialSectionData.secId": mongoose.Types.ObjectId(sectionData.secId),
            },
            { $set: {"materialSectionData.$": sectionData, updatedDate:Date.now()}},{new: true}).exec();

        if(!res)
        {
            return this.createSectionData(sectionData, uid, cid, mid, lid);
        }
        return res;
    }
}

module.exports = ThemeModel;
