const InternalError = require("./InternalError");

class EntityDuplicateKeyException extends InternalError
{

}

module.exports = EntityDuplicateKeyException;