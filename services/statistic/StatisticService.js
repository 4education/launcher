//this wrapper should incoupsulate all business logic
const StatisticModel = require("./models/StatisticModel");
const NotFoundEntityException = require("../core/exceptions/NotFoundEntityException");
const InternalErrorException = require("../core/exceptions/InternalError");

class StatisticService
{

    addTest(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { testsCount: 1 }});
    }
    addGroup(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { groupsCount: 1 }});
    }
    addTheme(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { themesCount: 1 }});
    }
    addPublishConf(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { pubConfCount: 1 }});
    }

    rmTest(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { testsCount: -1 }});
    }
    rmGroup(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { groupsCount: -1 }});
    }
    rmTheme(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { themesCount: -1 }});
    }
    rmPublishConf(cid)
    {
        return this.__getModel().updateCreateByCid({cid:cid, $inc: { pubConfCount: -1 }});
    }

    getAll()
    {
        return this.__getModel().getAll();
    }

    create(item)
    {
        return this.__getModel().create(item);
    }

    async getItem(id)
    {
        let data = await this.__getModel().getById(id);
        return data && data.toObject ? data.toObject() : null;
    }

    async getByCid(cid)
    {
        let data = await this.__getModel().getByCid(cid);
        return data && data.toObject ? data.toObject() : null;
    }

    async updateItem(id, item)
    {
        let product = await this.__getModel().getById(id);
        if(!product)
        {
            throw new NotFoundEntityException("Entity not found");
        }
        let updatedItem = await this.__getModel().update(id, item);

        if(!updatedItem)
        {
            throw new InternalErrorException("something wrong");
        }

        return updatedItem && updatedItem.toObject ? updatedItem.toObject() : null;
    }

    async deleteItem(id)
    {
        let item = await this.__getModel().getById(id);
        if(!item)
        {
            throw new NotFoundEntityException("Entity not found");
        }
        let updatedItem = await this.__getModel().remove(id);

        if(!updatedItem)
        {
            throw new InternalErrorException("something wrong");
        }

        return true ;
    }

    __getModel()
    {
        return new StatisticModel();
    }
}

module.exports = StatisticService;
