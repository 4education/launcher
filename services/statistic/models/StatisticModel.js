const mongoose = require('../../../libs/mongoose').mongoose;
const FilteredCollectionModel = require('../../core/models/FilteredCollectionModel');

// define the Statistic model schema
const StatisticSchema = new mongoose.Schema({
    cid:            { type: String, required: true, index: true},
    testsCount:     { type: Number, default: 0},
    groupsCount:   { type: Number, default: 0},
    pubConfCount:   { type: Number, default: 0},
    themesCount:    { type: Number, default: 0}
});

const provider = mongoose.model('statistics', StatisticSchema);

class StatisticModel extends FilteredCollectionModel
{

    create(itemData)
    {
        return provider.create(itemData);
    }

    update(id, itemData)
    {
        itemData.lastUpdatedDate = Date.now();
        return provider.findOneAndUpdate({_id: mongoose.Types.ObjectId(id)}, {$set: itemData}, {new: true}).exec();
    }

    getAll()
    {
        return provider.find().lean().exec();
    }

    remove(id)
    {
        return provider.find({_id: mongoose.Types.ObjectId(id)}).remove().exec();
    }

    getById(id)
    {
        return provider.findOne({_id: mongoose.Types.ObjectId(id)}).exec();
    }

    getByCid(id)
    {
        return provider.findOne({cid: mongoose.Types.ObjectId(id)}).exec();
    }

    updateCreateByCid(itemData)
    {
        const {cid} = itemData;
        return provider.findOneAndUpdate({cid:  mongoose.Types.ObjectId(cid)}, itemData, {new: true, upsert: true}).exec();
    }

    __getLookup()
    {
        return;
    }

    getProvider()
    {
        return provider;
    }
}

module.exports = StatisticModel;
