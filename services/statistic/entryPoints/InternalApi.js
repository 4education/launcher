const StatisticService = require("../StatisticService");

class InternalApi
{
    async getByCid(id)
    {
        return new StatisticService().getByCid(id);
    }
}

module.exports = InternalApi;