const serviceContainer = require("../../../libs/ServiceContainer");
const statService = require("../StatisticService")

class EventApi
{
    /**
     * @return {Object} Events Object contains all events which service fired. object have keys that are event names
     */
    getServiceEvents()
    {
        //return events;
    }

    async onEventDispatch(event)
    {
        const testEvents = serviceContainer.getService('events', 'testsService').getServiceEvents();
        const publEvents = serviceContainer.getService('events', 'publishConfig').getServiceEvents();
        const themeEvents = serviceContainer.getService('events', 'themes').getServiceEvents();

        console.log(event);
        switch(event.type)
        {
            case(testEvents.TEST_CREATED):
                new statService().addTest(event.data.cid);
                break;
            case(testEvents.TEST_DELETED):
                new statService().rmTest(event.data.cid);
                break;
            case(testEvents.GROUP_CREATED):
                new statService().addGroup(event.data.cid);
                break;
            case(testEvents.GROUP_DELETED):
                new statService().rmGroup(event.data.cid);
                break;
            case(publEvents.PUBLISH_CONF_CREATED):
                new statService().addPublishConf(event.data.cid);
                break;
            case(publEvents.PUBLISH_CONF_DELETED):
                new statService().rmPublishConf(event.data.cid);
                break;
            case(themeEvents.THEME_CREATED):
                new statService().addTheme(event.data.cid);
                break;
            case(themeEvents.THEME_DELETED):
                new statService().rmTheme(event.data.cid);
                break;
        }
    }
}

module.exports = EventApi;