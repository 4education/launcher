const InternalError = require("./InternalError");

class RequestQuery
{
    //TODO::validation by schema
    constructor(schema)
    {
        this.schema = schema;
        this.fields = {};
        this.whereConditions = {};
        this.paginationParams = {limit:null, skip:null};
        this.sortBy = {};
        this.groupBy = {};
        this.unwind = null;
        return this;
    }

    setExtractedField(field)
    {
        this.unwind = field;
    }

    setSelectFields(fields)
    {
        if(!Array.isArray(fields))
        {
            throw new InternalError("fields should be array");
        }
        let selectedFields = {};
        for (let i = 0; i < fields.length; i++)
        {
            selectedFields[fields[i]] = 1;
        }
        this.fields = selectedFields;
    }

    addSelectField(field, data)
    {
        if(typeof data !== "undefined")
        {
            this.fields[field] = data;
        }
        else
        {
            this.fields[field] = 1;
        }
    }

    addGroupByStage(groupByStage)
    {
        this.groupBy =  groupByStage;
    }

    addWhereConditions(field, data)
    {
        if(typeof data === 'object')
        {
            for(let key in data)
            {
                switch(key)
                {
                    case 'in':
                        if(!Array.isArray(data[key]))
                        {
                            throw new InternalError("Incorrect condition value. Allow *in* with array value only");
                        }
                        this.whereConditions[field] = {$in: data[key]};
                        break;
                    case 'not_in':
                        if(!Array.isArray(data[key]))
                        {
                            throw new InternalError("Incorrect condition value. Allow *not_in* with array value only");
                        }
                        this.whereConditions[field] = {$nin: data[key]};
                        break;
                    case 'like':
                        let val = new RegExp(data[key]);
                        if(typeof data[key] === "object")
                        {
                            if(!data[key].options || !data[key].pattern)
                            {
                                throw new InternalError("Incorrect condition value. Allow *like* with string or object with *pattern* and *options* only");
                            }
                            val = new RegExp(data[key].pattern, data[key].options)
                        }
                        this.whereConditions[field] = {$regex: val};
                        break;
                    case '<':
                        if(typeof data[key] !== "number")
                        {
                            throw new InternalError("Incorrect condition value. Allow *<* with array value only");
                        }
                        this.__addWhereCondition(field,  {$lt: data[key]});
                        break;
                    case '<=':
                        if(typeof data[key] !== "number" && !(data[key] instanceof Date))
                        {
                            throw new InternalError("Incorrect condition value. Allow *<=* with array value only");
                        }
                        this.__addWhereCondition(field, {$lte: data[key]});
                        break;
                    case '>':
                        if(typeof data[key] !== "number" && !(data[key] instanceof Date))
                        {
                            throw new InternalError("Incorrect condition value. Allow *>* with array value only");
                        }
                        this.__addWhereCondition(field, {$gt: data[key]});
                        break;
                    case '>=':
                        if(typeof data[key] !== "number" && !(data[key] instanceof Date))
                        {
                            throw new InternalError("Incorrect condition value. Allow *>=* with Date or Number value only");
                        }
                        this.__addWhereCondition(field, {$gte: data[key]});
                        break;
                    default:
                        throw new InternalError("This structure condition value not allow");
                }
            }
            return;
        }
        this.whereConditions[field] = data;
    }


    __addWhereCondition(field, data)
    {
        if(this.whereConditions[field])
        {
            this.whereConditions[field] = {...this.whereConditions[field], ...data};
        }
        else{
            this.whereConditions[field] = data;
        }
    }


    addWhereConditionsForCustomObject(field, data)
    {
        this.whereConditions[field] = data;
    }

    addSortBy(field, stringData)
    {
        switch (stringData)
        {
            case('desc'):
                this.sortBy[field] = -1;
                break;
            case('ask'):
                this.sortBy[field] = 1;
                break;
            default:
                throw new InternalError("Incorrect sort value. Allow ask/desc");
        }
    }

    setPagination(skip, limit)
    {
        this.paginationParams.skip  = skip;
        this.paginationParams.limit = limit;
    }

    getFindRequest()
    {

    }
}

module.exports = RequestQuery;
