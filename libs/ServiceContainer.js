//it's something like server discovery in microservice architecture

const serviceEntryPointsTypes = ['internal', 'api', 'events', 'cli'];
const INTERNAL = 'internal';
const API = 'api';
const events = 'events';

class ServiceContainer
{

    constructor()
    {
        this.servicesEntyPoints = {
            'internal': {}, 'api': {}, 'events':{}, 'cli': {}
        };
    }

    registerServiceEntryPoints(type, name, entryPoint) {
        if (serviceEntryPointsTypes.indexOf(type) === -1) {
            throw new Error(type + " it's not valid entry point type");
        }

        if (this.servicesEntyPoints[type][name]) {
            throw new Error(name + " impossible register service point twice");
        }
        this.servicesEntyPoints[type][name] = entryPoint;

    }

    getService(type, name)
    {
        return this.servicesEntyPoints[type] && this.servicesEntyPoints[type][name] ?
            this.servicesEntyPoints[type][name] : null;
    }

    getServicesByType(type)
    {
        return this.servicesEntyPoints[type] || {};
    }

    replaceServiceEntryPoints(type, name, entryPoint)
    {
        this.servicesEntyPoints[type][name] = entryPoint;
    }
}

module.exports = new ServiceContainer();