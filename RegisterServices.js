let serviceContainer = require("./libs/ServiceContainer");
//======================================================================================================================

//router Service
let routerService = require("./services/router/RouterService");
serviceContainer.registerServiceEntryPoints('internal', 'routerService', routerService);
//======================================================================================================================


//event service
let eventPointInternal = require("./services/event/entryPoints/Internal");
serviceContainer.registerServiceEntryPoints('internal', 'eventService',eventPointInternal);
//======================================================================================================================

//notification service
// let notificationPointInternal = require("./services/notification/entryPoints/Internal");
// let emailConf = require("./config/email");
// serviceContainer.registerServiceEntryPoints('internal', 'notificationService', new notificationPointInternal(emailConf));
//======================================================================================================================

//content services
 let ProgressApi = require("./services/progress/entryPoints/Api");

 serviceContainer.registerServiceEntryPoints('api', 'progressService', new ProgressApi());
//======================================================================================================================

//content services
let contentApi = require("./services/content/entryPoints/InternalApi");

serviceContainer.registerServiceEntryPoints('internal', 'contentService', new UsersApi());
//======================================================================================================================


//security service
let oauthInternal = require("./services/oauth/entryPoints/InternalApi");
routerService.setMiddleWareFunction(new oauthInternal().addUserDataToRequest);

let securityService = require("./services/security/entryPoints/Api");
routerService.setMiddleWareFunction(new securityService().processRequest);
//======================================================================================================================

